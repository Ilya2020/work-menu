#============================================================
import easygui
import datetime
import matplotlib.animation as animation
from math import *
import matplotlib.pyplot as plt
#============================================================
x = []
y = []
fig=plt.figure()
nemodar=fig.add_subplot(1,1,1)
r_s=25
r_m=20
r_h=15
r=27.5
daraje=60
adad=1
#============================================================
#============================================================
def file_new():
    while True:
        output = easygui.textbox(msg='Please write your text here')
        if output != None:
            while True:
                address = easygui.filesavebox(msg='Please save file where you want.')
                if address == None:
                    break
                else:
                    if address.endswith('.txt') == True:
                        f = open(address , 'w')
                        f.write(output)
                        f.close()
                        return
                    else:
                        easygui.msgbox('Yor file format is not txt.Please add .txt end of your file.')
        else:
            return
#============================================================
def file_edit():
    while True:
        address = easygui.fileopenbox(msg='Please select a text file that you want to edit.')
        if address == None:
            return
        else:
            if address.endswith('.txt') == True:
                f = open(address , 'r')
                data = f.read()
                f.close()
                output = easygui.textbox(msg=address , text=data)
                if output != None:
                    f = open(address , 'w')
                    f.write(output)
                    f.close()
                return
            else:
                easygui.msgbox('Please choose txt file.!')
#============================================================
def job_file():
    while True:
        choice = easygui.buttonbox('Mode:' , choices=['Create a new file','Edit file','Back'])    
        if choice == 'Create a new file':
            file_new()
        if choice == 'Edit file':
            file_edit()
        if choice == None or choice == 'Back':
            return
#============================================================
#============================================================
def plot_type():
    global x
    global y
    while True:
        data = easygui.multenterbox(msg='Please write your numbers here and split them with ",".',fields=['X','Y'])
        if data != None:
            while True:
                if data[0] == '' or data[1] == '':
                    easygui.msgbox(msg='Please write numbers in both X and Y')
                    data = easygui.multenterbox(fields=['X','Y'])
                else:
                    break
            x = []
            y = []
            xs = data[0].split(',')
            ys = data[1].split(',')
            while True:
                for i in range(len(xs)):
                    if type(xs[i])==str:
                        easygui.msgbox('Please write numbers nut strings.')
                        data=easygui.multenterbox(msg='Please write your numbers here and split them with ",".',fields=['X','Y'])
                    elif data[0] == '' or data[1] == '':
                        easygui.msgbox(msg='Please write numbers in both X and Y')
                        data = easygui.multenterbox(fields=['X','Y'])
                    else :
                        break
            xs = data[0].split(',')
            ys = data[1].split(',') 
            for X in xs:
                x.append(int(X))
            for Y in ys:
                y.append(int(Y))
            return
        else:
            return
#============================================================
def plot_draw():
    global x
    global y

    plot_color = 'b'
    plot_marker = 'o'
    plot_line = '-'

    c = {'red':'r','green':'g','blue':'b',None:'b'}
    m = {'star':'*','circle':'o','square':'s','diamond':'D',None:'o'}
    l = {'line':'-','dash':'--','dash dot':'-.','dot':':',None:'-'}
    while True:
        choice = easygui.choicebox(msg='You can choose what you want to change.',choices=['Draw plot','Plot color','Plot marker','Plot line Style'])
        if choice == 'Plot color':
            sub_choice = easygui.choicebox(msg='Witch color do you want?',choices=['red','green','blue'])
            plot_color = c[sub_choice]
        if choice == 'Plot marker':
            sub_choice = easygui.choicebox(msg='Witch marker do you want?',choices=['star','circle','square','diamond'])
            plot_marker = m[sub_choice]
        if choice == 'Plot line Style':
            sub_choice = easygui.choicebox(msg='Witch line style do you want?',choices=['line','dash','dash dot','dot'])
            plot_line = l[sub_choice]
        if choice == 'Draw plot':
            plt.plot(x,y,color=plot_color,marker=plot_marker,linestyle=plot_line)
            plt.show()
            return
        if choice == None:
            return
#============================================================
def plot_file():
    global x
    global y
    x=[]
    y=[]
    adad=[]
    while True:
        address=easygui.fileopenbox(msg='Choose file address')
        if address==None :
            return
        while True:
            if address.endswith('.txt')==False:
                easygui.msgbox(msg='Please choose txt file.')
                address=easygui.fileopenbox(msg='Choose file address')
            else:
                break
        f=open(address,'r')
        f=f.read()
        f=f.splitlines()
        for i in range(len(f)):
            adad=(f[i].split(','))
            x.append(adad[0])
            y.append(adad[1])
            if x[i]=='':
                easygui.msgbox(msg='Please write all of x plots')
                break
            elif y[i]=='':
                easygui.msgbox(msg='Please write all of y plots')
                break
        return
#============================================================
def job_plot():
    while True:
        choice = easygui.buttonbox('Please choose what you want to do with plot' , choices=['Type numbers for plot','Read numbers from a file for plot','Draw plot','Back'])
        if choice == 'Type numbers for plot':
            plot_type()
        elif choice == 'Read numbers from a file for plot':
            plot_file()
        elif choice == 'Draw plot':
            plot_draw()
        elif choice == None or choice == 'Back':
            return
#============================================================
#============================================================
def clock_saat():
    global nemodar
    global r
    global r_h
    global r_m
    global r_s
    nemodar.axis([0,80,0,80])
    nemodar.set_aspect("equal")
    nemodar.add_artist(plt.Circle((40,40),30,fill=False,color='darkorange'))
    name1=[40,40+(r*cos(radians(60)))]
    name2=[40,40+(r*sin(radians(60)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'1',fontsize=20)
    name1=[40,40+(r*cos(radians(30)))]
    name2=[40,40+(r*sin(radians(30)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'2',fontsize=20)
    name1=[40,40+(r*cos(radians(0)))]
    name2=[40,40+(r*sin(radians(0)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'3',fontsize=20)
    name1=[40,40+(r*cos(radians(-30)))]
    name2=[40,40+(r*sin(radians(-30)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'4',fontsize=20)
    name1=[40,40+(r*cos(radians(-60)))]
    name2=[40,40+(r*sin(radians(-60)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'5',fontsize=20)
    name1=[40,40+(r*cos(radians(-90)))]
    name2=[40,40+(r*sin(radians(-90)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'6',fontsize=20)
    name1=[40,40+(r*cos(radians(-120)))]
    name2=[40,40+(r*sin(radians(-120)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'7',fontsize=20)
    name1=[40,40+(r*cos(radians(-150)))]
    name2=[40,40+(r*sin(radians(-150)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'8',fontsize=20)
    name1=[40,40+(r*cos(radians(180)))]
    name2=[40,40+(r*sin(radians(180)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'9',fontsize=20)
    name1=[40,40+(r*cos(radians(150)))]
    name2=[40,40+(r*sin(radians(150)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'10',fontsize=20)
    name1=[40,40+(r*cos(radians(120)))]
    name2=[40,40+(r*sin(radians(120)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'11',fontsize=20)
    name1=[40,40+(r*cos(radians(90)))]
    name2=[40,40+(r*sin(radians(90)))]
    nemodar.text(name1[-1]-1,name2[-1]-1,'12',fontsize=20)
    plt.title('Clock')
#============================================================
def clock_animate(i):
    global nemodar
    global r_h
    global r_m
    global r_s
    time=datetime.datetime.now()
    second=time.second
    minute=time.minute
    hour=time.hour
    x=[40,40+(r_s*cos(radians(second*-6+90)))]
    y=[40,40+(r_s*sin(radians(second*-6+90)))]
    x1=[40,40+(r_m*cos(radians(-((second+minute*60)/10)+90)))]
    y1=[40,40+(r_m*sin(radians(-((second+minute*60)/10)+90)))]
    x2=[40,40+(r_h*cos(radians((hour*3600+minute*60+second)/-120+90)))]
    y2=[40,40+(r_h*sin(radians((hour*3600+minute*60+second)/-120+90)))]
    nemodar.clear()
    clock_saat()
    nemodar.plot(x,y,color='deepskyblue',linestyle='-.',label='second')
    nemodar.plot(x1,y1,color='limegreen',linestyle=':',label='minute')
    nemodar.plot(x2,y2,color='orchid',label='hour')
    plt.legend()
#============================================================
def job_clock():
    anim=animation.FuncAnimation(fig,clock_animate,interval=1000)
    plt.show()       
#============================================================
#============================================================
def job_calculator():
    javab=0
    number=0
    while True:
        number1=''
        number2=''
        numbers=easygui.enterbox(msg=f'Answer is {javab} || Please write two numbers with one of these operators: "*" "/" "-" "+"')
        if numbers == None:
            return
        numbers=list(numbers)
        for i in range(len(numbers)):
            if numbers[i]=='+' or numbers[i]=='*' or numbers[i]=='/' or numbers[i]=='-':
                number=i
                for l in range(number):
                    number1+=numbers[l]
                for k in range(len(numbers)-number-1):
                    number2+=numbers[k+number+1]
        if number==0:
            easygui.msgbox(msg=f'Please write an operator from these operators: "*" "/" "-" "+"')
        if number1.isnumeric()==True and number2.isnumeric()==True:
            if numbers[number]=='+':
                javab=int(number1)+int(number2)
            elif numbers[number]=='*':
                javab=int(number1)*int(number2)
            elif numbers[number]=='/':
                javab=int(number1)/int(number2)
            elif numbers[number]=='-':
                javab=int(number1)-int(number2)
        elif number1.isnumeric()==False and number1!='' or number2.isnumeric()==False and number2!='':
            easygui.msgbox(msg='Please write numbers not strings.')
        
#============================================================
#============================================================
while True:
    job = easygui.choicebox(msg='Please chosse what you want to do' , choices=['Work with plot','Work with file','Show live clock','Calculate two numbers'])
    if job == 'Work with file':
        job_file()
    elif job == 'Work with plot':
        job_plot()
    elif job == 'Calculate two numbers':
        job_calculator()
    elif job == 'Show live clock':
        job_clock()
    elif job == None:
        break